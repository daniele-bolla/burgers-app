import React from 'react';
import CreateOrder from './components/createOrder';
import ShowOrders from './components/showOrders';


class App extends React.Component {
  render() {
    return (
      <div>
          <CreateOrder />
					<ShowOrders />
      </div>
    );
  }
}

export default App;
