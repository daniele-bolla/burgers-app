export const FILL_BOOT =  'boots:Fill';
export const CLEAR_BOOT =  'boots:Clear';
export const SELECT_BOOT =  'boots:Select';
export const CREATE_BOOT =  'boots:Create';

export function fillBoot(payload, id){
	return {
		type: FILL_BOOT,
		payload: {...payload, order:payload.order, active:true, selected:true},
		id
	}
}
export function selectBoot(payload){
	return {
		type: SELECT_BOOT,
		payload: {...payload, active:true, selected:true},
	}
}
export function deSelectBoots(payload, id){
	const boot = Object.assign({},payload,{active:true, selected:true})
	return {
		type: CLEAR_BOOT,
		payload:boot,
		id
	}
}
