export const CREATE_ORDER =  'orders:Create';
export const EDIT_ORDER =  'orders:Edit';
export const REMOVE_ORDER =  'orders:Remove';

export function createOrder(payload, id){
	return {
			type: CREATE_ORDER,
			payload,
			id
	}
}

export function editOrder(payload, id){
	return {
			type: CREATE_ORDER,
			payload,
			id
	}
}

export function removeOrder(payload, id){
	return {
			type: CREATE_ORDER,
			payload,
			id
	}
}