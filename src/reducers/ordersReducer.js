import { CREATE_ORDER } from "../actions/ordersActions";

const initialState = {
  byId: [],
  byHash: {}
};

export default function ordersReducer(
  state = initialState,
  { type, payload, id }
) {
  switch (type) {
    case CREATE_ORDER: {
      return {
        byId: [...state.byId, id],
        byHash: {
          ...state.byHash,
          [id]: { ...payload, id }
        }
      };
    }
    default: {
      return state;
    }
  }
}
