import { SELECT_BOOT } from '../actions/bootsActions';

const initialState = null;

const currentBootReducer = (state = initialState, {type, payload, id}) => {
	switch(type){
		case SELECT_BOOT: {
			return payload
		}  
		default: {
			return state
		}
	}
}
export default currentBootReducer;