import React from "react";
import { connect } from "react-redux";
import { createOrder } from "../actions/ordersActions";
import PropTypes from 'prop-types';

const initialState = {
	showForm: false,
	order:{
		number: 0,
		type: "",
		dishes: [],
		total: 0
	}
};

const resetState = (state, props) => (initialState);
const totalList = (arr, key) => arr.reduce((acc, item) => acc + item[key], 0);

class CreateOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState
	}
	componentWillReceiveProps(nextProps){
    this.setState(resetState);
	}
  onCreateOrder = e => {
    e.preventDefault();
    this.props.onCreateOrder(this.state.order, "#order-" + Math.random());
  };
  showForm = e => {
    e.preventDefault();
    this.setState({ show: true });
  };
  handleChange = event => {
    const {target:{ name, value }} = event;
		const updatedState = {...this.state, order: {...this.state.order, [name]: value }};
    this.setState((state, props) => updatedState);
  };
  selectDish = (event, dish) => {
		event.preventDefault();
		const dishes = [...this.state.order.dishes, dish];
		const total = totalList(dishes, "price");
		const updatedState = {...this.state, order: {...this.state.order, dishes, total}};
    this.setState((state, props) => updatedState);
  };
  render() {
		const dishes = Object.values(this.props.dishes.byHash);
		/**Style */
    const orderCss = {
			flexBasis: "fill",
			flexGrow: 2,
      border: "solid 5px #222",
      padding: "2em",
      margin: "2em"
		};
		const flexBox = {
			display:"flex",
			flexBasis: "fit-content"

		};
		const flexColumn = {
			border: "solid 5px #222",
      padding: "2em",
      margin: "2em",
			flexBasis: "fit-content",
			flexGrow: 1,

    };
    return (
      <div style={flexBox}>
				<div style={flexColumn}>
					<form>
						<fieldset>
							<label htmlFor="type">* Type:</label>
							<select
								id="type"
								name="type"
								aria-required="true"
								value={this.state.order.type}
								onChange={this.handleChange}
							>
								<option value="table">Table</option>
								<option value="takeaway">Takeaway</option>
							</select>
						</fieldset>
						<fieldset>
							<label htmlFor="number">* Number:</label>
							<input
								id="number"
								name="number"
								type="number"
								min="0"
								aria-required="true"
								value={this.state.order.number}
								onChange={this.handleChange}
							/>
						</fieldset>
						<fieldset>
							<ul>
								{dishes.filter(comment => comment).map((dish, i) => (
									<li
										key={i}
										onClick={e => {
											this.selectDish(e, dish);
										}}
									>
										{dish.name}
									</li>
								))}
							</ul>
						</fieldset>
					</form>
					<button
						onClick={e => {
							this.onCreateOrder(e);
						}}
					>
						Print
					</button>
				</div>
        <div style={orderCss}>
          <h1>Order</h1>
          <div>{this.state.order.type}</div>
          <div>{this.state.order.number}</div>
          <div>
            {" "}
            <ul>
              {this.state.order.dishes.filter(comment => comment).map((dish, i) => (
                <li key={i}>{dish.name}</li>
              ))}
            </ul>
            <div>{this.state.order.total}</div>
          </div>
        </div>
      </div>
    );
  }
}
/**
 * Redux
 */
const mapDispatchToProps = dispatch => {
  return {
    onCreateOrder: (order, id, callback) => dispatch(createOrder(order, id))
  };
};
const mapStateToProps = (state, props) => {
  return {
    orders: state.orders,
    dishes: state.dishes
  };
};
/**Typechecking */
CreateOrder.propTypes = {
  name: PropTypes.string
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateOrder);


