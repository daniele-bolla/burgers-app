import { FILL_BOOT, CREATE_BOOT, CLEAR_BOOT } from '../actions/bootsActions';

const initialState = {
	byId: ['#table_1', '#table_2', '#table_3'],
	byHash: {
	'#table_1': {id: '#table_1', order: {}, active: false, selected: false, type:"table"},
	'#table_2': {id: '#table_2', order: {}, active: false, selected: false, type:"table"},
	'#table_3': {id: '#table_3', order: {}, active: false, selected: false, type:"table"},
	'#table_4': {id: '#table_3', order: {}, active: false, selected: false, type:"table"},
	'#table_5': {id: '#table_3', order: {}, active: false, selected: false, type:"table"},
	'#takeaway_1': {id: '#takeaway_1', order: {}, active: false, selected: false, type:"takeaway"},
	'#takeaway_2': {id: '#takeaway_2', order: {}, active: false, selected: false, type:"takeaway"},
	'#takeaway_3': {id: '#takeaway_3', order: {}, active: false, selected: false, type:"takeaway"}
	}
}

const bootsReducer = (state = initialState, {type, payload, id}) => {
	switch(type){
		case CREATE_BOOT: {
			return {
				byId: [ ...state.byId, id],
				byHash: {
					...state.byHash,
					[id]: payload
				}
			}
		}
		case FILL_BOOT: {
			state.byHash[id] = {
			...state.byHash[id],
			...payload
			}
			return {...state,  payload}
		}  
		case CLEAR_BOOT: {
			const prunedIds = state.byId.filter(item => {
				return item !== id
			})
			delete state.byHash[id]
			return {
				byId: prunedIds,
				byHash: state.byHash
			}
		}
		default: {
			return state
		}
	}
}
export default bootsReducer;