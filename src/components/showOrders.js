import React from "react";
import { connect } from "react-redux";

class ShowOrders extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
		const orders = Object.values(this.props.orders.byHash);
		/**Style */
		const orderCss = {
      border: "solid 5px #222",
      padding: "0.4em"
		};
		const gridBox = {
			display: "grid",
			width: "100%",
			gridTemplateColumns: 'repeat(auto-fit, minmax("200px", "250px"))',
			gridAutoRows: "1fr",
			gridGap: "1em"

		};
		const flexColumn = {
			border: "solid 5px #222",
      padding: "2em",
      margin: "2em",
			flexBasis: "fit-content",
			flexGrow: 1,
    };
    return (
      <div style={gridBox}>
          {orders.filter(order => order).map((order, i) => (
            <div style={orderCss} key={i} >
              {order.id}
							<ul>
								{order.dishes.filter(dish => dish).map((dish, i) => (
									<li
										key={i}
										onClick={e => {
											this.selectDish(e, dish);
										}}
									>
										{dish.name}
									</li>
								))}
								</ul>
            </div>
          ))}
      </div>
    );
  }
}


const mapStateToProps = (state, props) => {
  return {
    orders: state.orders
  };
};

export default connect(mapStateToProps)(ShowOrders);
