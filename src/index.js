import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';
import bootsReducer from './reducers/bootsReducers';
import ordersReducer from './reducers/ordersReducer';
import dishesReducer from './reducers/dishesReducer';


const reducers = combineReducers({
	orders: ordersReducer,
	boots: bootsReducer,
	dishes: dishesReducer
})

const store = createStore(reducers, {
	orders:{
		byId: [],
		byHash: {}
	},
	dishes:{
		byId:[],
		byHash:{
			'hand_of_the_king': {
				id:'hand_of_the_king',
				name:'Hand of the King',
				price:8
			},
			'birds_of_feather': {
				id:'birds_of_feather',
				name:'Birds o Feather',
				price:8.50
			},
			'birds_of_feroger_rabbitather': {
				id:'roger_rabbit',
				name:'Roger Rabbit',
				price:9
			},
		}
	},
	boots:{
		byId: ['#table_1', '#table_2', '#table_3'],
		byHash: {
		'#table_1': {
			'id': '#table_1',
			'order': {
				'boot': '#table_1',
				'id': null,
				'dishes': [
					{
						'name': 'Hand of the King',
						'price': 8
					},
					{
						'name': 'Birds of Feather',
						'price': 8.5
					}
				],
				'total': 16.5
			},
			'active': true,
			'selected': true
		},
		'#table_2': {id: '#table_2', order: {}, active: false, selected: false, type:'table'},
		'#table_3': {id: '#table_3', order: {}, active: false, selected: false, type:'table'},
		'#table_4': {id: '#table_4', order: {}, active: false, selected: false, type:'table'},
		'#table_5': {id: '#table_5', order: {}, active: false, selected: false, type:'table'},
		'#takeaway_1': {id: '#takeaway_1', order: {}, active: false, selected: false, type:'takeaway'},
		'#takeaway_2': {id: '#takeaway_2', order: {}, active: false, selected: false, type:'takeaway'},
		'#takeaway_3': {id: '#takeaway_3', order: {}, active: false, selected: false, type:'takeaway'}
		}
	}
},
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
      <Provider store={store}><App /></Provider>, document.getElementById('root'));

registerServiceWorker();
